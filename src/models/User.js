import {db, dataType} from './../config/database'

const User = db.define('user', {
  firstName: {
    type: dataType.STRING
  },
  lastName: {
    type: dataType.STRING
  },
  email: {
    type: dataType.STRING,
    unique: true,
    allowNull: false,
    isEmail: true
  }
})

export default User
