var express = require('express')

const route = express.Router()

route.get('/', (req, res) => {
  res.render('index.html')
})

route.get('/about', (req, res) => {
  res.render('about.html')
})

route.get('/contact', (req, res) => {
  res.render('contact.html')
})

module.exports = route
