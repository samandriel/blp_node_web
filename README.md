การรัน Project
---
1. ไปที่โปรเจ็คแล้ว พun `gulp watch`
2. เบราเซอร์จะเปิดขึ้นมาอัตโนมัติ ที่ `http://localhost:3000` สามารถแก้ไขงานได้โดยไม่ต้อง Refresh หน้าเว็บ

การแก้ไขไฟล์งาน
---
- ให้ทำการแก้ไขงานในโฟเดอร์ `src` ไฟล์ทั้งหมดจะถูก Compile ไปไว้ในโฟลเดอร์ `public`
- สามารถก๊อปปี้ โฟลเดอร์ `public` ไปอัพขึ้นโฮสได้ทันที

โครงสร้างไฟล์หลัง
---
1. `src`
    - html ใน `src` จะใช้ syntax ของ template engine (nunjucks) เพื่อลดความซ้ำซ้อนของโค๊ด (Syntax ไม่ยาก สามารถสังเกตุได้จากการใช้งาน)
    - `src/partial` เก็บโค้ดที่สามารถนำเอาไปใช้ในหลายๆหน้าของเว็บเพจได้ เช่น Navbar Footer เป็นต้น
    - `src/layout` ใช้เก็บ Layout ของเว็บ
    - ใช้เมื่อต้องการโฮสเว็บกับ netlify.com เท่านั้น 

2. `src/assets` ใช้เก็บไฟล์ Asset ต่างๆ
    - `js` เก็บโค้ด javascript ใช้ ES6 และ ESlint-standard syntax (สามารถใช้ js ธรรมดาเขียนได้แต่ไม่ต้องมี ; หลัง)
    - `stylus` เก็บโค้ด css ใช้ stylus syntax (Syntax ไม่ยาก สามารถสังเกตุได้จากการใช้งาน)
    - `images` เก็บรูป
    - `font` เก็บ font

อ่านเพิ่มเติม
---
[nunjucks](https://mozilla.github.io/nunjucks/templating.html)
[ES6](https://www.babelcoder.com/blog/posts/introduction-to-es2015)(ภาษาไทย)
[stylus](http://stylus-lang.com/docs/selectors.html)